# CI documentation

Contents has been imported from https://wiki.inria.fr/ciportal/ with
the script import_mediawiki/import_mediawiki.py

https://inria-ci.gitlabpages.inria.fr/doc

## Serve the documentation locally with hugo

Hugo must be pre-installed on your machine, then:

```bash
hugo --config localconfig.toml server
```

```txt
Web Server is available at http://localhost:1313/doc/ (bind address 0.0.0.0)
Press Ctrl+C to stop
```

## Serve the documentation inside a docker container

```bash
docker pull klakegg/hugo:0.107.0 # Not mandatory if already pulled
docker run --rm -it -v $(pwd):/src -p 1313:1313 klakegg/hugo:0.107.0 server
```

```txt
Web Server is available at http://localhost:1313/doc/ (bind address 0.0.0.0)
Press Ctrl+C to stop
```
