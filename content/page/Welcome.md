---
title: Welcome to Inria's CI platform
---

Dear user,

Welcome to Inria's Continuous Integration platform, ci.inria.fr

To get started with the platform, see its documentation available at:

https://ci.inria.fr/doc/

Need help? You can:
 * Ask questions on the Mattermost channel, https://mattermost.inria.fr/ci/channels/town-square, or
 * Contact the support team by e-mail at ci-support@inria.fr


Happy hacking,

Inria's CI platform support team.
