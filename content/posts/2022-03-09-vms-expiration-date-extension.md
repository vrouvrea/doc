+++
title =  "VMs expiration date extension"
date = "2022-03-09T12:39:58+01:00"
tags = ["featured"]
+++

You may have received this morning some emails inviting you to extend
the expiration date of your CI virtual machines. As you may have
noticed, the URLs were wrong: `/prolongation` was missing at the
end. You may either use the fixed URL, or the "Extends" button in the
slave table in the web interface of your CI project.

Sorry for this inconvenience, and thank you for extending your VMs!
