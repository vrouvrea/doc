+++
title =  "Login failure on CI portal and CloudStack on 2023/04/05: LDAP certificate expired"
date = "2023-04-05T15:20:45+02:00"
tags = ["featured"]
+++

Dear CI users,

The LDAP certificate for ci.inria.fr has been renewed: the certificate
was expired on 2023/04/05 and login was impossible on that day on CI
portal (ci.inria.fr) and CloudStack (login on virtual machines still
worked). You should no longer have difficulties to login on CI
anymore.

Sorry for the inconvenience.
