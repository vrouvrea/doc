+++
title =  "VM expiration: 1 year, renewable"
date = "2021-03-23T14:26:06+01:00"
tags = ["featured"]
+++
Each VM hosted by ci.inria.fr has now an expiration date, fixed in one
year from now for current VMs, and one year after the creation date
for future VMs. This rule will make us able to clean unused VMs and
save ressources for new projects.

These expiration dates are freely renewable, using the dedicated
button "Extend" in the VM list associated to a project in the portal
ci.inria.fr.

When VMs are about to expire, project admins will be reminded three
times by email (with a link to extend the expiration date) starting
three weeks before the expiration. Expired VMs will be deleted and
project admins will be notified (there will be a 24h delay before
expunging, during which the VM can be recovered).

Thank you for your understanding!
