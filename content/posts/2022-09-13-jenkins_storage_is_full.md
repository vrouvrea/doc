+++
title =  "Jenkins storage is full"
date = "2022-09-13T14:10:47+02:00"
tags = ["featured"]
+++

The shared storage for Jenkins instance is full: this probably
prevents all Jenkins instances to work properly. We are currently
investigating why we have difficulties to free space and I hope we
will be able to restore the service very soon!
