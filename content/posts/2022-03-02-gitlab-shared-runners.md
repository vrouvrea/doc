+++
title =  "Shared docker runners for gitlab.inria.fr"
date = "2022-03-08T12:21:19+01:00"
tags = ["featured"]
+++

We are pleased to announce that shared docker runners are now
available for gitlab.inria.fr!

Thanks to everyone who helped testing the service during the (very
long!) beta period. You can right now enable the shared runners in the
CI/CD settings of your Gitlab projects.

See the
[shared runners documentation](https://ci.inria.fr/doc/page/gitlab/#using-shared-runners-linux-only)
for more details.
The three following docker runners are available:

- small (1 CPU, 2048 MB RAM), tags: ci.inria.fr, linux, small
- medium (2 CPUs, 4096 MB RAM), tags: ci.inria.fr, linux, medium
- large (4 CPUs, 8096 MB RAM), tags: ci.inria.fr, linux, large

The runners will handle jobs declared in the `.gitlab-ci.yml` file of
the project repository, at the condition that the jobs have at least
one tag among `ci.inria.fr`, `linux` and either `small`, `medium` or
`large` (these three last tags are exclusive: they cannot be used
together for the same job since no runner will match more than one of
them).

We would like to send a big thank you to Anthony Baire for all his
efforts in developping this service, and we would like to thank also
the CI infrastructure team of the Inria DSI who helped in deploying
the platform.

Thank you very much for using ci.inria.fr, and happy hacking!
