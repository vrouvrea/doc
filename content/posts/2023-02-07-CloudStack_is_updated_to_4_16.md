+++
title =  "CloudStack is now updated to 4.16"
date = "2023-01-30T15:25:06+01:00"
tags = ["featured"]
+++

CloudStack is now updated to 4.16. You should now be able to start
your virtual machines again via the portal ci.inria.fr.

You may refer to the portal documentation: https://inria-ci.gitlabpages.inria.fr/doc/page/web_portal_tutorial/#slaves

We still have a problem that prevents users to log in to CloudStack by
the web interface. Only the CloudStack web interface seems to be
affected: you are still able to manage your virtual machines via the
portal or the CloudStack API. We will fix the connection to the web
interface as soon as possible.

Thank you and sorry for the inconvenience.
