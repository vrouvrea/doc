+++
title =  "Jenkins storage is back to normal"
date = "2022-09-19T10:55:48+02:00"
tags = ["featured"]
+++

The shared storage for Jenkins instance is back to normal since last
Thursday. If your Jenkins instance is down due to disk space outage,
restarting the instance should now work again. Sorry for the
inconvenience, we will try in the next few days to take
counter-measures to prevent these kinds of problems in the future.
