+++
title =  "Web portal maintenance, the 30th of March 2021 18:00-18:30"
date = "2021-03-30T14:26:06+01:00"
tags = []
+++

The web portal ci.inria.fr will be unavailable the 30th of March 2021
from 18:00 till 18:30 for maintenance purposes.
It concerns only the web portal. CloudStack, virtual machines, Jenkins
and gitlab-ci shall not be impacted.

Thank you for your understanding!
