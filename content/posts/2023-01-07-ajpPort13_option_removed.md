+++
title =  "Jenkins option `--ajpPort13=-1` removed"
date = "2023-01-07T12:46:33+01:00"
tags = ["featured"]
+++

The option `--ajpPort13=-1` has been removed for launching
Jenkins. This option was necessary for very old version of Jenkins,
but was ignored by all currently deployed Jenkins instance, and is no
longer supported since Jenkins 2.375 (released on 2022-10-25). If you
experienced failure while updating Jenkins recently, you should have a
try now. Thank you!

Happy new year!
