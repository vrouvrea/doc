+++
title =  "VMs will be interrupted on Tuesday afternoon 2023-02-07, 2pm-6pm, for CloudStack maintenance"
date = "2023-01-30T15:25:06+01:00"
tags = ["featured"]
+++

All VMs hosted on ci.inria.fr will be interrupted on Tuesday afternoon
2023-02-07, 2pm-6pm, for CloudStack maintenance.
CloudStack will be upgraded to 4.16, and nested KVM will be enabled.
The actual interruption may be shorter, we will publish an announce
when the platform will be back.
You will have to restart the VMs manually via ci.inria.fr or CloudStack.
Thank you!
